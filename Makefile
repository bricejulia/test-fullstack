SYMFONY			= symfony
SYMFONY_CONSOLE	= $(SYMFONY) console
COMPOSER		= $(SYMFONY) composer

##
## Project
## -------
##

start: ## Start the project
	-$(SYMFONY) server:start -d --no-tls

stop:
	$(SYMFONY) server:stop

clean: ## Stop the project and remove generated files
	rm -rf vendor

clear-cache: ## Clear cache data
	rm -rf var/cache/de*

dump-env-var:
	$(SYMFONY) var:export --multiline

php: ## Run as interactive shell
	$(SYMFONY) php -a

.PHONY: start stop clean clear-cache dump-env-var php

##
## Utils
## -----
##

db: vendor
	-$(SYMFONY_CONSOLE) doctrine:database:create --if-not-exists
	$(SYMFONY_CONSOLE) doctrine:migrations:migrate --no-interaction --allow-no-migration

migration: ## Generate a new doctrine migration
migration: vendor
	$(SYMFONY_CONSOLE) doctrine:migrations:diff

apply-migration: ## Run doctrine migration
apply-migration: vendor
	$(SYMFONY_CONSOLE) doctrine:migrations:migrate --no-interaction --allow-no-migration

db-validate-schema: ## Validate the doctrine ORM mapping
db-validate-schema: vendor
	$(SYMFONY_CONSOLE) doctrine:schema:validate

bundle-assets: ## Copy bundle assets
bundle-assets: vendor
	$(SYMFONY_CONSOLE) assets:install

composer.lock: composer.json
	$(COMPOSER) update --lock --no-scripts --no-interaction

vendor: composer.lock
	$(COMPOSER) install

composer-update:
	$(COMPOSER) update

.PHONY: bundle-assets composer.lock vendor composer-update
.DEFAULT_GOAL := help

help:
	@grep -E '(^[a-zA-Z_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'
.PHONY: help
