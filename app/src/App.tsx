import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import IndexPage from './page/index';

const App: React.FC = () => (
  <BrowserRouter>
    <Switch>
      <Route path="/" exact component={IndexPage} />
    </Switch>
  </BrowserRouter>
);

export default App;
