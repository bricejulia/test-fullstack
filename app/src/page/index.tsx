import React from 'react';

function IndexPage() {
  return (
    <div>
      <p>Hello from index page</p>
    </div>
  );
}

export default IndexPage;
