## Fullstack test with Symfony/API platform and Typescript and react.js

This interview aims at evaluating your current symfony and typescript skills and/or your learning capabilities.

### Requirements

* Symfony binary, visit [https://symfony.com/download](https://symfony.com/download) to download and install it
* PHP 7.4
* nodejs > 10.x
* composer 2
* yarn / npm
* git
* optional: make

### API Goals

* Clone this repo and install its dependencies (`make vendor` or `symfony composer install` or `composer install`)
* Run `symfony server:start -d --no-tls` or `make start` and visit [http://127.0.0.1:8000]( http://127.0.0.1:8000) to see the result
* Create one security user (can be `in_memory` or `entity`) with role `ROLE_END_USER`
* Create a new security user with role `ROLE_REVIEWER`
* Set up a JWT authentication process (like [LexikJWTAuthenticationBundle](https://github.com/lexik/LexikJWTAuthenticationBundle))
* Enable json login form in Symfony Security which accept an email and password as body
* Create an [API platform](https://api-platform.com/) resource `Book` (int id AUTO GENERATED, title string length 255, author string length 255) which allow GET/PUT/POST verbs
* Create an [API platform](https://api-platform.com/) resource `Review` (int id AUTO GENERATED, body text, rating smallint, book BookId) which allow GET/PUT/POST verbs
* role `ROLE_END_USER` can
    * GET / PUT / POST `Book`
    * GET `Review`
* role `ROLE_REVIEWER` can
    * GET `Book`
    * GET / PUT / POST `Review`
* Create a `/api/profile` route that return the logged in user (username, roles)
* Ideas:
    * route `/api/books` need to return a collection of books with the average rating
    * add database migrations
    * Add phpunit to test your API

### APP Goals

* The goal here is to show off your react/typescript skills, but a nice integration is also welcomed
* Go to directory `app`
* install its dependencies (`npm install` or `yarn`)
* Run `npm run dev` or `yarn dev` and visit [http://localhost:3000]( http://localhost:3000) to see the result
* Set up the styling solution of your choice (styled-components, SASS, any framework...) or simply use builtin css import
* Create two simple pages : login and dashboard

### Login page

![](https://i.imgur.com/D7ena7N.jpg)

* Create a simple form with email and password (you can use [formik](https://github.com/jaredpalmer/formik) and [yup](https://github.com/jquense/yup) but a simple form is also fine !)
* When submitting the form, send a request to your API route using either the `fetch` API or any package of your choice ([axios](https://github.com/axios/axios)...)
* If credentials are wrong, display a simple error message of your choice
* If they're valid, [navigate](https://reactrouter.com/web/guides/quick-start) to the dashboard page

### Dashboard page

* Create a component that displays books with their reviews
* Ideas:
    * Create a form to create/edit book
    * Create a form to create/edit a review
    * add a logout button

### Notes

* Clone this repo and push it into one of your own on GitHub or GitLab (any host is fine)
* Be creative ! You can add some features if you want and/or want to display some of your skills
